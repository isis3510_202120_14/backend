import boto3
import logging
import uuid
import json
# Importa las dependencias de logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

""" --- Functions that put a stadistic in the database --- """
def register_stadistic(intent_request):
    logger.debug('in register_stadistic')
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('db-cloudbot-stadistics')
    #Create a random UUID... this will the sample element we add to the dynamo db.
    p_uuid = uuid.uuid4().hex 
    # Get the message from the dynamo db
    response = table.put_item(
        Item={
            "id": "{}".format(p_uuid),
            "service": "{}".format(intent_request["service"]),
            "payload": intent_request["payload"],
            "fecha": "{}".format(intent_request["fecha"]),
        }
    )
    logger.debug('response in the dynamo: {}'.format(response))
    return response
    
# --- Main handler ---
def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    logger.debug('event.stadistics={}'.format(event))
    return register_stadistic(event)