import * as cdk from "@aws-cdk/core";
import * as dynamodb from "@aws-cdk/aws-dynamodb";
import * as lambda from "@aws-cdk/aws-lambda";
//  Construct que se encarga de construir los elementos que conforman el servicio de estadisticas
export class StadisticsConstruct extends cdk.Construct { 
  // permite acceder a la funcion standBy que se crea dentro del construct
  public readonly lambdaStadistics: lambda.Function;
  // Constructor de la clase
  constructor(scope: cdk.Construct, id: string) {
    super(scope, id);
    // ========================================================================
    // Resource: Lambda Function
    // ========================================================================
    // Funcion que pone un elemento de tipo stadistic dentro de la base de datos
    this.lambdaStadistics=new lambda.Function(
      this,
      "StadisticsFunctionTecho",
      {
        functionName:"stadistics-function-techo",
        runtime: lambda.Runtime.PYTHON_3_8,
        code: lambda.Code.fromAsset("lambda-stadistics"),
        handler: "lambda_function_stadistics.lambda_handler"
      }
    );
    // ========================================================================
    // Resource: Dynamo DB Table
    // ========================================================================
    const tableName = "db-techo-stadistics";
    //Crea la tabla que contiene los mensajes a mostrar en cada flujo
    const table = new dynamodb.Table(this, "dynamoStadistics", {
      // Nombre de la tabla
      tableName: tableName,
      // Define la pk
      partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
      //Destruye la tabla con cdk destroy
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });
    // da permisos de lectura/escritura de la funcion a la lambda
    table.grantReadWriteData(this.lambdaStadistics);
  }
}