import * as cdk from "@aws-cdk/core";
import { AthenaConnectionConstruct } from "./athena-connection-construct";
import { CognitoCdk } from "./cognito-cdk";

export class BackTechoStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Componente que crea el userpool de Cognito
    new CognitoCdk(this, "cognito-productos");

    //Componente que permite crear la conexion con athena
    new AthenaConnectionConstruct(this, "athena-connection-construct", {
      account: this.account,
      region: this.region,
    });
  }
}
