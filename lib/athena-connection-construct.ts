import * as athena from "@aws-cdk/aws-athena";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as s3 from "@aws-cdk/aws-s3";
import * as cdk from "@aws-cdk/core";
/**
 * Construct que se encarga de realizar la conexion entre quicksight
 * y la dynamo db-cloubot-stadistics por medio del servicio aws Athena
 */
export interface AthenaConnectionProps {
  /**
   * Cuenta de aws proporcionada por el stack
   */
  account: string;
  /**
   *  Region donde se esta desplegando el stack
   */
  region: string;
}

/**
 * CDK Clase que genera toda la conexion entre Athena y Quicksight
 */
export class AthenaConnectionConstruct extends cdk.Construct {
  //Constructor de la clase
  constructor(scope: cdk.Construct, id: string, props: AthenaConnectionProps) {
    super(scope, id);
    /**
     *  Bucket s3 que almacena los datos que exceden los limites de respuesta
     *  de la función lambda
     */
    const spillbucketTechoApp = new s3.Bucket(this, "SpillBucketTechoApp", {
      bucketName: "spillbucket-techoapp",
      autoDeleteObjects: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });
    /**
     * Bucket s3 que almacena las querys realizadas por el motor de Athena
     */
    const outputBucketTechoApp = new s3.Bucket(this, "outputBucketTechoApp", {
      bucketName: "outputbucket-techoapp",
      autoDeleteObjects: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    // Rol que maneja los permisos de quicksight
    // const qsrole = iam.Role.fromRoleArn(
    //   this,
    //   "QuickSightRoleImport",
    //   `arn:aws:iam::${props.account}:role/service-role/aws-quicksight-s3-consumers-role-v0`
    // );

    // //Permite que quicksight realice la invocacion de la lambda athenadbconnector
    // qsrole.addToPrincipalPolicy(
    //   new iam.PolicyStatement({
    //     actions: ["lambda:InvokeFunction"],
    //     resources: ["*"],
    //   })
    // );
    // //Permite que quicksight vea todos lo buckets y realice acciones sobre ellos
    // qsrole.addToPrincipalPolicy(
    //   new iam.PolicyStatement({
    //     actions: ["s3:ListAllMyBuckets"],
    //     resources: ["*"],
    //   })
    // );
    // qsrole.addToPrincipalPolicy(
    //   new iam.PolicyStatement({
    //     actions: [
    //       "s3:ListBucket",
    //       "s3:ListBucketMultipartUploads",
    //       "s3:GetBucketLocation",
    //     ],
    //     resources: [
    //       spillbucketTechoApp.bucketArn,
    //       outputBucketTechoApp.bucketArn,
    //     ],
    //   })
    // );
    // qsrole.addToPrincipalPolicy(
    //   new iam.PolicyStatement({
    //     actions: [
    //       "s3:GetObject",
    //       "s3:GetObjectVersion",
    //       "s3:PutObject",
    //       "s3:AbortMultipartUpload",
    //       "s3:ListMultipartUploadParts",
    //     ],
    //     resources: [
    //       spillbucketTechoApp.bucketArn + "/*",
    //       outputBucketTechoApp.bucketArn + "/*",
    //     ],
    //   })
    // );
    // Crea el workgroup de athena
    new athena.CfnWorkGroup(this, "workgroup-techoapp", {
      name: "workgroup-techoapp",
      recursiveDeleteOption: true,
      workGroupConfiguration: {
        resultConfiguration: {
          outputLocation: outputBucketTechoApp.s3UrlForObject(),
        },
        engineVersion: {
          selectedEngineVersion: "Athena engine version 2",
        },
      },
    });

    // // AthenaDynamoDBConnector Lambda creada desde el template de cloudformation athenadbconnector.yaml
    const connector = lambda.Function.fromFunctionArn(
      this,
      "dbconnector-techoapp",
      `arn:aws:lambda:${props.region}:${props.account}:function:workgroup-techoapp`
    );

    // // Crea el catalogo de athena para poder realizar la introquicksight
    new athena.CfnDataCatalog(this, "datacatalog", {
      name: "dbconnector-techoapp",
      type: "LAMBDA",
      parameters: {
        function: connector.functionArn,
      },
    });
  }
}
