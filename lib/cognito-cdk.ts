import * as cdk from "@aws-cdk/core";
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";

export class CognitoCdk extends cdk.Construct {
  constructor(scope: cdk.Construct, id: string) {
    super(scope, id);

    // ========================================================================
    // Resource: Amazon Cognito User Pool
    // ========================================================================
    const userPool = new cognito.UserPool(this, "UserPool", {
      userPoolName: "userpool-techo-cdk",
      selfSignUpEnabled: true, // Allow users to sign up
      userVerification: {
        emailSubject: "Verify your email for TECHO app",
        emailBody:
          "Thanks for signing up to our TECHO app Your verification code is {####}",
        emailStyle: cognito.VerificationEmailStyle.CODE,
        smsMessage:
          "Thanks for signing up to our TECHO Your verification code is {####}",
      },
      autoVerify: { email: true }, // Verify email addresses by sending a verification code
      standardAttributes: {
        email: {
          required: true,
          mutable: false,
        },
      },
      signInAliases: { email: true },
      signInCaseSensitive: false,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });
    const userPoolClient = new cognito.UserPoolClient(this, "UserPoolClient", {
      userPool,
      generateSecret: false, // Don't need to generate secret for web app running on browsers
    });

    // ========================================================================
    // Resource: Amazon Cognito Identity Pool
    // ========================================================================
    const identityPool = new cognito.CfnIdentityPool(this, "IdentityPool", {
      identityPoolName: "identitypool-techo-cdk",
      allowUnauthenticatedIdentities: true,
      cognitoIdentityProviders: [
        {
          clientId: userPoolClient.userPoolClientId,
          providerName: userPool.userPoolProviderName,
        },
      ],
    });

    // ========================================================================
    // Resource: Amazon Cognito Identity Pool Roles
    // ========================================================================
    const isAnonymousCognitoGroupRole = new iam.Role(
      this,
      "anonymous-group-techo-role",
      {
        description: "Default role for anonymous users",
        assumedBy: new iam.FederatedPrincipal(
          "cognito-identity.amazonaws.com",
          {
            StringEquals: {
              "cognito-identity.amazonaws.com:aud": identityPool.ref,
            },
            "ForAnyValue:StringLike": {
              "cognito-identity.amazonaws.com:amr": "unauthenticated",
            },
          },
          "sts:AssumeRoleWithWebIdentity"
        ),
        managedPolicies: [
          iam.ManagedPolicy.fromAwsManagedPolicyName(
            "service-role/AWSLambdaBasicExecutionRole"
          ),
        ],
      }
    );

    const isUserCognitoGroupRole = new iam.Role(
      this,
      "users-group-techo-role",
      {
        description: "Default role for authenticated users",
        assumedBy: new iam.FederatedPrincipal(
          "cognito-identity.amazonaws.com",
          {
            StringEquals: {
              "cognito-identity.amazonaws.com:aud": identityPool.ref,
            },
            "ForAnyValue:StringLike": {
              "cognito-identity.amazonaws.com:amr": "authenticated",
            },
          },
          "sts:AssumeRoleWithWebIdentity"
        ),
        managedPolicies: [
          iam.ManagedPolicy.fromAwsManagedPolicyName(
            "service-role/AWSLambdaBasicExecutionRole"
          ),
        ],
      }
    );

    // ========================================================================
    // Resource: Amazon Cognito Identity Pool Roles
    // ========================================================================
    new cognito.CfnIdentityPoolRoleAttachment(
      this,
      "identity-pool-role-techo-attachment",
      {
        identityPoolId: identityPool.ref,
        roles: {
          authenticated: isUserCognitoGroupRole.roleArn,
          unauthenticated: isAnonymousCognitoGroupRole.roleArn,
        },
        roleMappings: {
          mapping: {
            type: "Token",
            ambiguousRoleResolution: "AuthenticatedRole",
            identityProvider: `cognito-idp.${
              cdk.Stack.of(this).region
            }.amazonaws.com/${userPool.userPoolId}:${
              userPoolClient.userPoolClientId
            }`,
          },
        },
      }
    );
    // Outputs
    new cdk.CfnOutput(this, "UserPoolId", {
      value: userPool.userPoolId,
    });
    new cdk.CfnOutput(this, "UserPoolClientId", {
      value: userPoolClient.userPoolClientId,
    });
  }
}
