/* eslint-disable no-irregular-whitespace */
import * as cdk from "@aws-cdk/core";
import * as lambda from "@aws-cdk/aws-lambda";
import * as apigw from "@aws-cdk/aws-apigateway";
// Interfaz que define las propiedades de la clase
export interface ApiTechoProps {
  /**
   * Funcion encargada de obtener y manejar la informacion del usuario
   */
  // functionUser: lambda.IFunction;
  /**
   * Funcion encargada de obtener y manejar la información de las tareas
   */
   // functionTask: lambda.IFunction;
   /**
   * Funcion encargada de obtener y manejar la información de los eventos
   */
  // functionEvents: lambda.IFunction;
   /**
   * Funcion encargada de obtener y manejar la informacion de los learning paths
   */
   // functionLearningPath: lambda.IFunction;
  /**
   * Funcion encargada de obtener y manejar la informacion de la Media
   */
  functionStadistics: lambda.IFunction;
   /**
   * Funcion encargada de obtener y manejar la informacion de la Media
   */
   // functionMedia: lambda.IFunction;
  /**
   * Nombre del stack
   */
  stackName: string;
}
// Clase que define el Apigateway del backend de techo
export class ApiGatewayTecho extends cdk.Construct {
  /**
   * Funcion privada que crea un endpoint que asocia una funcion lambda con el api gateway
   * @param api Api gateway a asociar la lambda
   * @param stackName Nombre del stack
   * @param endpointName Nombre del endopoint a crear dentro del api gateway
   * @param lambda funcion lambda a asociar con el api gateway
   */
  private createEndpointLambda(
    api: apigw.RestApi,
    stackName: string,
    endpointName: string,
    lambda: lambda.IFunction
  ) {
    // Crea el endpoint
    const endpoint = api.root.addResource(`${stackName}-${endpointName}`);
    // Crea la integracion de las respuestas del Lambda, con un encabezado CORS
    const IntegrationLambda = new apigw.LambdaIntegration(lambda, {
      proxy: false,
      integrationResponses: [
        {
          // Respuesta exitosa de la función Lambda, sin filtro definido
          // - el filtro selectionPattern solo prueba el mensaje de error
          // Estableceremos el código de estado de respuesta en 200
          statusCode: "200",
          responseParameters: {
            // Podemos mapear los parámetros de respuesta
            // - Los parámetros de destino (la clave) son los parámetros de respuesta (usados ​​en mapeos)
            // - Los parámetros de origen (el valor) son los parámetros o expresiones de respuesta de integración
            "method.response.header.Content-Type": "'application/json'",
            "method.response.header.Access-Control-Allow-Origin": "'*'",
            "method.response.header.Access-Control-Allow-Credentials": "'true'",
          },
        },
        {
          // Para errores, verificamos si el mensaje de error no está vacío, obtenemos los datos del error
          selectionPattern: "(\n|.)+",
          // Estableceremos el código de estado de respuesta en 200
          statusCode: "400",
          responseParameters: {
            "method.response.header.Content-Type": "'application/json'",
            "method.response.header.Access-Control-Allow-Origin": "'*'",
            "method.response.header.Access-Control-Allow-Credentials": "'true'",
          },
        },
      ],
    });
    // Añade el metodo post y dentro incluye a los metodos de respuesta del api gateway con encabezados CORS
    endpoint.addMethod("POST", IntegrationLambda, {
      methodResponses: [
        {
          // Respuesta exitosa de la integración
          statusCode: "200",
          // Definir qué parámetros están permitidos o no
          responseParameters: {
            "method.response.header.Content-Type": true,
            "method.response.header.Access-Control-Allow-Origin": true,
            "method.response.header.Access-Control-Allow-Credentials": true,
          },
        },
        {
          // Lo mismo para las respuestas de error
          statusCode: "400",
          responseParameters: {
            "method.response.header.Content-Type": true,
            "method.response.header.Access-Control-Allow-Origin": true,
            "method.response.header.Access-Control-Allow-Credentials": true,
          },
        },
      ],
    });
  }
  constructor(scope: cdk.Construct, id: string, props: ApiTechoProps) {
    super(scope, id);
    // ========================================================================
    // Resource: Amazon API Gateway
    // ========================================================================
    const api = new apigw.RestApi(this, `${props.stackName}-api`, {
      defaultCorsPreflightOptions: {
        // Define respuestas OPTIONS a todos los origenes
        allowOrigins: apigw.Cors.ALL_ORIGINS,
      },
      restApiName: `${props.stackName}-service`,
    });

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint stadistics 
    // ========================================================================
    // Crea el endpoint para el manejo de las estadisticas dentro del backend serverless
    this.createEndpointLambda(
      api,
      props.stackName,
      "stadistics",
      props.functionStadistics
    );

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint User
    // ========================================================================
    // Crea el endpoint para el manejo de los usuarios dentro del backend serverless
    // this.createEndpointLambda(
    //   api,
    //   props.stackName,
    //   "user",
    //   props.functionUser
    // );

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint task
    // ========================================================================
    // Crea el endpoint para el manejo de las tasks dentro del backend serverless
    // this.createEndpointLambda(
    //   api,
    //   props.stackName,
    //   "task",
    //   props.functionTask
    // );

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint Events
    // ========================================================================
    // Crea el endpoint para el manejo de los events dentro del backend serverless
    // this.createEndpointLambda(
    //   api,
    //   props.stackName,
    //   "events",
    //   props.functionEvents
    // );

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint Learning Path
    // ========================================================================
    // Crea el endpoint para el manejo de los learning path dentro del backend serverless
    // this.createEndpointLambda(
    //   api,
    //   props.stackName,
    //   "learningPath",
    //   props.functionLearningPath
    // );

    // ========================================================================
    // Resource: Amazon API Gateway Endpoint Media
    // ========================================================================
    // Crea el endpoint para el manejo de los archivos media dentro del backend serverless
    // this.createEndpointLambda(
    //     api,
    //     props.stackName,
    //     "Media",
    //     props.functionMedia
    // );

    // ========================================================================
    // Resource: CFN Outputs
    // ========================================================================
    // muestra en consola el endpoint para manejar stadistics
    new cdk.CfnOutput(this, "Endpoint-stadistics", {
      exportName: "endpoint-stadistics",
      value: `${props.stackName}-stadistics`,
    });
    // muestra en consola el endpoint para manejar users
    new cdk.CfnOutput(this, "Endpoint-user", {
      exportName: "endpoint-user",
      value: `${props.stackName}-user`,
    });
    // muestra en consola el endpoint para manejar tasks
    new cdk.CfnOutput(this, "Endpoint-task", {
      exportName: "endpoint-task",
      value: `${props.stackName}-task`,
    });
    // Muestra en consola el endpoint para manejar events
    new cdk.CfnOutput(this, "Endpoint-events", {
      exportName: "endpoint-events",
      value: `${props.stackName}-events`,
    });
    // Muestra en consola el endpoint para manejar events
    new cdk.CfnOutput(this, "Endpoint-learningPath", {
      exportName: "endpoint-learningPath",
      value: `${props.stackName}-learningPath`,
    });
    // Muestra en consola el endpoint para manejar Media
    new cdk.CfnOutput(this, "Endpoint-Media", {
      exportName: "endpoint-media",
      value: `${props.stackName}-media`,
    });
  }
}