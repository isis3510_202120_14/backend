#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { BackTechoStack } from '../lib/back-techo-stack';

const app = new cdk.App();
new BackTechoStack(app, 'BackTechoStack');
